----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.12.2020 17:12:32
-- Design Name: 
-- Module Name: freq_divider_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity freq_divider_tb is
--  Port ( );
end freq_divider_tb;

architecture Behavioral of freq_divider_tb is
    Component freq_divider is    
		Generic(
            FREQ : positive := 1000
        );
        Port(
            CLK : in std_logic;
            CLK_OUT : out std_logic
        );
	end Component;
	
	--entradas
	signal CLK: std_logic;
	
	--salidas
    signal CLK_OUT: std_logic;       
    
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000; 
        
begin		
	uut: freq_divider    
	    Generic map(
	       FREQ => 1000
	        )	
    	Port map(
        	CLK => CLK,
            CLK_OUT => CLK_OUT
            );
        
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PER;
        CLK <= '1';
        wait for 0.5 * CLK_PER;
    end process;    
    
    tester : process
    begin        
        wait for 3 sec;        
        
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;
    
end Behavioral;