----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2021 10:30:05
-- Design Name: 
-- Module Name: seven_segment_mux_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity seven_segment_mux_tb is
--  Port ( );
end seven_segment_mux_tb;

architecture Behavioral of seven_segment_mux_tb is
    
    Component seven_segment_mux is    
		Generic(
            SIZE_IN : positive:= 4;
            SIZE_AN : positive:= 7;
            SIZE_CODE : positive:= 4
        );  
        Port(
            CLK : in std_logic; 
            RESET_N : in std_logic;
            N_0 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_1 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_2 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_3 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_4 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_5 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_6 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            N_7 : in std_logic_vector(SIZE_IN DOWNTO 0); 
            AN : out std_logic_vector(SIZE_AN DOWNTO 0); 
            CODE : out std_logic_vector(SIZE_CODE DOWNTO 0) 
        );
	end Component;
	
	--entradas
    signal CLK: std_logic;   
	signal RESET_N: std_logic;
    signal N_0: std_logic_vector(4 DOWNTO 0);
    signal N_1: std_logic_vector(4 DOWNTO 0);
    signal N_2: std_logic_vector(4 DOWNTO 0);
    signal N_3: std_logic_vector(4 DOWNTO 0);
    signal N_4: std_logic_vector(4 DOWNTO 0);
    signal N_5: std_logic_vector(4 DOWNTO 0);
    signal N_6: std_logic_vector(4 DOWNTO 0);
    signal N_7: std_logic_vector(4 DOWNTO 0);
    
    --salidas
    signal AN: std_logic_vector(7 DOWNTO 0);   
    signal CODE: std_logic_vector(4 DOWNTO 0);    
   
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000; 
        
begin		
	uut: seven_segment_mux  
	    Generic map(
            SIZE_IN => 4,
            SIZE_AN => 7,
            SIZE_CODE => 4
        ) 	
    	Port map (
    	    CLK => CLK,
        	RESET_N => RESET_N,
            N_0 => N_0,
            N_1 => N_1,
            N_2 => N_2,
            N_3 => N_3,  
            N_4 => N_4,
            N_5 => N_5,
            N_6 => N_6,
            N_7 => N_7,          
            AN => AN,        
            CODE => CODE
        );
        
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PER;
        CLK <= '1';
        wait for 0.5 * CLK_PER;
    end process;
    
    RESET_N <= '0' after 0.25 * CLK_PER, '1' after 0.75 * CLK_PER;
    
    tester : process
    begin        
    	wait until RESET_N = '1';
    	
    	--00.00
    	N_0 <= "00000";
        N_1 <= "00000";
        N_2 <= "00001";
        N_3 <= "00000";  
        
        --P1
        N_7 <= "10110";
        N_6 <= "00010";
        N_5 <= "11000";
        N_4 <= "11000";          
        wait for 10 * CLK_PER; 
         
    	--00.10
    	N_0 <= "00000";
        N_1 <= "00010";
        N_2 <= "00001";
        N_3 <= "00000";
        
        --P2
        N_7 <= "10110";
        N_6 <= "00100";
        N_5 <= "11000";
        N_4 <= "11000";                 
        wait for 10 * CLK_PER;
        
        --00.20
        N_0 <= "00000";
        N_1 <= "00100";
        N_2 <= "00001";
        N_3 <= "00000";   
        
        --P3
        N_7 <= "10110";
        N_6 <= "00110";
        N_5 <= "11000";
        N_4 <= "11000";             
        wait for 10 * CLK_PER;
        
        --00.30
        N_0 <= "00000";
        N_1 <= "00110";
        N_2 <= "00001";
        N_3 <= "00000";  
        
        --P4
        N_7 <= "10110";
        N_6 <= "01000";
        N_5 <= "11000";
        N_4 <= "11000";           
        wait for 10 * CLK_PER;
        
        --00.40
        N_0 <= "00000";
        N_1 <= "01000";
        N_2 <= "00001";
        N_3 <= "00000";
        
        --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";                 
        wait for 10 * CLK_PER;
        
        --00.50
        N_0 <= "00000";
        N_1 <= "01010";
        N_2 <= "00001";
        N_3 <= "00000";   
        
         --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";               
        wait for 10 * CLK_PER;
        
        --00.60
        N_0 <= "00000";
        N_1 <= "01100";
        N_2 <= "00001";
        N_3 <= "00000";  
        
         --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";                
        wait for 10 * CLK_PER;
        
        --00.70
        N_0 <= "00000";
        N_1 <= "01110";
        N_2 <= "00001";
        N_3 <= "00000";  
        
         --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";                
        wait for 10 * CLK_PER;
        
        --00.80
        N_0 <= "00000";
        N_1 <= "10000";
        N_2 <= "00001";
        N_3 <= "00000";  
        
         --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";                
        wait for 10 * CLK_PER;
        
        --00.90
        N_0 <= "00000";
        N_1 <= "10010";
        N_2 <= "00001";
        N_3 <= "00000";   
        
         --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";               
        wait for 10 * CLK_PER;
        
        --01.00
        N_0 <= "00000";
        N_1 <= "00000";
        N_2 <= "00011";
        N_3 <= "00000";
        
         --vac�o
        N_7 <= "11000";
        N_6 <= "11000";
        N_5 <= "11000";
        N_4 <= "11000";                
        
        wait for 0.25 * CLK_PER;
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;
end Behavioral;
