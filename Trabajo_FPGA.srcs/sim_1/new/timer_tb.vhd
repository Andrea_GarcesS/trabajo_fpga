----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2020 10:16:35
-- Design Name: 
-- Module Name: timer_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity timer_tb is
--  Port ( );
end timer_tb;

architecture Behavioral of timer_tb is

    Component timer is
        
        Generic(
            FREQ : positive := 1000;
            MAX_TIME : positive := 10;
            SIZE_TRIGGER : positive := 3;
            TRIGGER_VALUE : std_logic_vector(3 DOWNTO 0) := "1000"
        );    
		Port(
            CLK : in std_logic;
            RESET_N : in std_logic;
            TRIGGER : in std_logic_vector(SIZE_TRIGGER DOWNTO 0);
            TIMER_FLAG : out std_logic
        );
	end component;		
	
	--entradas
    signal CLK: std_logic; 
    signal RESET_N: std_logic;  
    signal TRIGGER: std_logic_vector(3 DOWNTO 0);
    
    --salidas
    signal TIMER_FLAG: std_logic; 
    
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000;
        
begin		
	uut: timer	
	   Generic map(
	        FREQ => 1000,
    		MAX_TIME => 2,
    		SIZE_TRIGGER => 3,
    		TRIGGER_VALUE => "1000"
    	)	      	
    	Port map(
        	CLK => CLK,
            RESET_N => RESET_N,
            TRIGGER => TRIGGER,
            TIMER_FLAG => TIMER_FLAG
        );
        
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PER;
        CLK <= '1';
        wait for 0.5 * CLK_PER;
    end process;    
    
    RESET_N <= '0' after 0.25 * CLK_PER, '1' after 0.75 * CLK_PER;
    
    tester : process
    begin        
        wait until RESET_N = '1';
        wait for 250 ms;
        TRIGGER <= "1000"; --hay disparo
        wait for 2 sec;
        TRIGGER <= "0001"; --no hay disparo
        wait for 15 sec;
        TRIGGER <= "1000"; --hay disparo
        wait for 2 sec;
        TRIGGER <= "0001"; --no hay disparo 
        wait; 
    end process;
    
end Behavioral;
