----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2021 16:36:14
-- Design Name: 
-- Module Name: debouncer_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer_tb is
--  Port ( );
end debouncer_tb;

architecture Behavioral of debouncer_tb is
    Component debouncer is
        Port(
            CLK : in  std_logic;
            BUTTON : in  std_logic;
            RESULT : out std_logic
        ); 
    end component;
    
    --entradas
    signal CLK: std_logic; 
    signal BUTTON: std_logic := '0';  
    
    --salidas
    signal RESULT: std_logic;
    
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000;
    
begin
    uut: debouncer port map (
        	CLK => CLK,
            BUTTON => BUTTON,
            RESULT => RESULT
        );
    clkgen : process
        begin
    	    CLK <= '0';
            wait for 0.5 * CLK_PER;
            CLK <= '1';
            wait for 0.5 * CLK_PER;
        end process; 
        
     tester : process
     begin        
        wait for 100 ms;
        BUTTON <= '1';
        wait for 5 ms; --pulso =< 20 ms -> no v�lido
        BUTTON  <= '0';
        wait for 50 ms; --pulso >= 20 ms -> v�lido
        BUTTON  <= '1';
        wait for 20 ms; --pulso >= 20 ms -> v�lido
        BUTTON  <= '0';
        
        wait for 50 ms;
        assert false
            report "[SUCCESS]: simulation finished."
            severity failure;
     end process;

end Behavioral;
