----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2020 12:27:08
-- Design Name: 
-- Module Name: decoder_int_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity encoder_coin_tb is
--  Port ( );
end encoder_coin_tb;

architecture Behavioral of encoder_coin_tb is
    Component encoder_coin
        Generic(
            SIZE_IN : positive := 3;
            SIZE_OUT : positive := 2
        );
        Port(
            BUTTON : in std_logic;
            SW : in std_logic_vector(SIZE_IN DOWNTO 0);
            COIN : out std_logic_vector(SIZE_OUT DOWNTO 0)       
        );
    end Component;
    
    --entradas
    signal BUTTON : std_logic; 
    signal SW : std_logic_vector (3 DOWNTO 0); 
    
    --salidas
    signal COIN : std_logic_vector (2 DOWNTO 0);         
   
begin
    uut : encoder_coin 
        Generic map(
            SIZE_IN => 3,
            SIZE_OUT => 2
        )
        Port map(
            BUTTON => BUTTON,
            SW => SW,
            COIN => COIN
            );    
    
    bttn: process
    begin
        BUTTON <= '0';
        wait for 20 ns;
        BUTTON <= '1';
        wait for 20 ns;
    end process;  
      
    tb: process
    begin
        wait for 200 ns;
        SW <= "0001"; --1�
        wait for 200 ns;
        SW <= "0010"; --50c
        wait for 200 ns;
        SW <= "0101"; --no v�lido
        wait for 200 ns;
        SW <= "0100"; --20c
        wait for 200 ns;
        SW <= "1000"; --10c
        
        wait for 200 ns;        
        assert false
            report "[SUCCESS]: simulation finished." 
            severity failure;
    end process;
end Behavioral;