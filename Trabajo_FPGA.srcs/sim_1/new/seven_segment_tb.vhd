----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2021 10:14:35
-- Design Name: 
-- Module Name: seven_segment_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity seven_segment_tb is
--  Port ( );
end seven_segment_tb;

architecture Behavioral of seven_segment_tb is

    component seven_segment is 
        Generic(
            SIZE_IN : positive := 3;
            SIZE_OUT : positive := 7;
            FREQ : positive := 200            
        );
        Port(
            CLK : in std_logic;
            RESET_N : in std_logic;
            COIN: in  std_logic_vector(SIZE_IN  DOWNTO 0);
            PRODUCTO: in  std_logic_vector(SIZE_IN  DOWNTO 0);
            AN : out std_logic_vector(SIZE_OUT  DOWNTO 0);
            LED : out std_logic_vector(SIZE_OUT  DOWNTO 0)            
        );
	end component;
	
	--entradas
    signal CLK: std_logic;   
	signal RESET_N: std_logic;
	signal COIN: std_logic_vector(3 DOWNTO 0);
	signal PRODUCTO: std_logic_vector(3 DOWNTO 0);
    
    --salidas
    signal AN: std_logic_vector(7 DOWNTO 0);   
    signal LED: std_logic_vector(7 DOWNTO 0);    
   
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000; 
        
begin		
	uut: seven_segment  
	    Generic map(
	       SIZE_IN => 3,
	       SIZE_OUT => 7,
	       FREQ => 200
	       )  	
    	Port map(
            CLK => CLK,
            RESET_N => RESET_N,                   
            COIN => COIN,
            PRODUCTO => PRODUCTO,
            AN => AN,        
            LED => LED
            );
        
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PER;
        CLK <= '1';
        wait for 0.5 * CLK_PER;
    end process;
    
    RESET_N <= '0' after 0.25 * CLK_PER, '1' after 0.75 * CLK_PER;
    
    tester : process
    begin        
    	wait until RESET_N = '1';     	
    		
    	wait for 15 ms;  
    	--00.00
    	COIN <= "0000";
    	--P1
        PRODUCTO <= "1000";
        wait for 30 ms;  
        
    	--00.10
    	COIN <= "0001" ;
    	--P2
    	PRODUCTO <= "0100";        
        wait for 30 ms;
        
        --00.20
        COIN <= "0010";
        --P3
        PRODUCTO <= "0010";        
        wait for 30 ms;
        
        --00.30
        COIN <= "0011";
        --P4
        PRODUCTO <= "0001";        
        wait for 30 ms;
        
        --00.40
        COIN <= "0100";
        --P1
        PRODUCTO <= "1000";        
        wait for 30 ms;
        
        --00.50
        COIN <= "0101";
        --P2
        PRODUCTO <= "0100";        
        wait for 30 ms;
        
        --00.60
        COIN <= "0110";
        --P3
        PRODUCTO <= "0010";        
        wait for 30 ms;
        
        --00.70
        COIN <= "0111";
        --P4
        PRODUCTO <= "0001";        
        wait for 30 ms;
        
        --00.80
        COIN <= "1000";
        --P1
        PRODUCTO <= "1000";        
        wait for 30 ms;
        
        --00.90
        COIN <= "1001";
        --P2
        PRODUCTO <= "0100";        
        wait for 30 ms;
        
        --01.00
        COIN <= "1010";
        --P3
        PRODUCTO <= "0010";
        
        --m�s de 1�
        COIN <= "1011";
        --P4
        PRODUCTO <= "0001";    
        
        wait for 50 ms;
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;
end Behavioral;