----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2021 13:06:52
-- Design Name: 
-- Module Name: decoder_bcd_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_bcd_tb is
--  Port ( );
end decoder_bcd_tb;

architecture Behavioral of decoder_bcd_tb is
    Component decoder_bcd is    
        Generic(
            SIZE_IN : positive:= 4;
            SIZE_OUT : positive := 7
        );
        
        Port (
            CODE : in std_logic_vector(SIZE_IN  DOWNTO 0);
            LED : out std_logic_vector(SIZE_OUT DOWNTO 0)       
        );
	end Component;
	
	--entradas
    signal CODE: std_logic_vector(4  DOWNTO 0);   
	
    --salidas
    signal LED: std_logic_vector(7 DOWNTO 0);          
        
begin		
	uut: decoder_bcd  
	    Generic map(
            SIZE_IN => 4,
            SIZE_OUT => 7
        ) 	
    	Port map (
    	    CODE => CODE,
        	LED => LED            
        );	
    
    tester : process
    begin        
    	wait for 5 ms;
    	CODE <= "00000"; --0
    	wait for 5 ms;
    	CODE <= "00001"; --0.
    	wait for 5 ms;
    	CODE <= "00010"; --1
    	wait for 5 ms;
    	CODE <= "00110"; --3
    	wait for 5 ms;
    	CODE <= "11101"; --A.        
        
        wait for 20 ms;
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;
    
end Behavioral;
