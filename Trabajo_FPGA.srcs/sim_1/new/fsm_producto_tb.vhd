----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.12.2020 12:25:54
-- Design Name: 
-- Module Name: fsm_producto_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_producto_tb is
--  Port ( );
end fsm_producto_tb;

architecture Behavioral of fsm_producto_tb is
     
	component fsm_producto is    
		Generic(
            SIZE_PRODUCTO : positive := 2;
            SIZE_SELECT : positive := 3;
            SIZE_LIGHT : positive := 2;
            SELECT_START_VALUE : std_logic_vector(3 DOWNTO 0) := "1010"
        );
        Port(
            RESET_N : in std_logic;
            CLK : in std_logic;
            PRODUCTO : in std_logic_vector(SIZE_PRODUCTO DOWNTO 0); 
            SELECT_START : in std_logic_vector(SIZE_SELECT DOWNTO 0);
            TIMER_OK : in std_logic;
            TIMER_ERROR : in std_logic;        
            LIGHT : out std_logic_vector(SIZE_LIGHT DOWNTO 0)
        );
	end Component;
	
	--entradas
	signal RESET_N: std_logic;
    signal CLK: std_logic;   
    signal PRODUCTO: std_logic_vector(2 DOWNTO 0);
    signal SELECT_START: std_logic_vector(3 DOWNTO 0);
    signal TIMER_OK: std_logic;
    signal TIMER_ERROR: std_logic;      
    
    --salidas  
    signal LIGHT: std_logic_vector(2 DOWNTO 0);    
   
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000; 
        
begin		
	uut: fsm_producto    	
    	Generic map(
            SIZE_PRODUCTO => 2,
            SIZE_SELECT => 3,
            SIZE_LIGHT => 2,
            SELECT_START_VALUE => "1010"
        )
    	Port map (
        	RESET_N => RESET_N,
            CLK => CLK,
            PRODUCTO => PRODUCTO,
            SELECT_START => SELECT_START,
            TIMER_OK => TIMER_OK,
            TIMER_ERROR => TIMER_ERROR,       
            LIGHT => LIGHT
        );
        
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PER;
        CLK <= '1';
        wait for 0.5 * CLK_PER;
    end process;
    
    RESET_N <= '0' after 0.25 * CLK_PER, '1' after 0.75 * CLK_PER;
    
    tester : process
    begin        
    	wait until RESET_N = '1';  
    	SELECT_START <= "1010"; --se puede seleccionar producto
    	wait for 10 * CLK_PER;        
        PRODUCTO <= "001"; --PRODUCTO 1
        wait for 5 * CLK_PER;
        TIMER_OK <= '1'; --finalización del temporizador timer_ok
        wait for 1.3 * CLK_PER;
        TIMER_OK <= '0';
        PRODUCTO <= "010"; --PRODUCTO 2
        wait for 3.3 * CLK_PER;
        TIMER_ERROR <= '1'; --finalización del temporizador timer_error
        wait for 1.1 * CLK_PER;
        TIMER_ERROR <= '0'; 
        PRODUCTO <= "110"; --producto no válido
        wait for 2.9 * CLK_PER;
        TIMER_OK <= '1'; --finalización del temporizador timer_ok
        wait for 1.7 * CLK_PER;
        TIMER_OK <= '0';
        wait for 2.5 * CLK_PER;
        SELECT_START <= "1000"; --no se puede seleccionar producto
        wait for 15 * CLK_PER;
        PRODUCTO <= "011"; --PRODUCTO 3
        wait for 1.7 * CLK_PER;
        TIMER_OK <= '1'; --finalización del temporizador timer_ok
        wait for 1.2 * CLK_PER;
        TIMER_OK <= '0';
        PRODUCTO <= "100"; --PRODUCTO 4
        wait for 4.1 * CLK_PER;
        TIMER_ERROR <= '1'; --finalización del temporizador timer_error
        wait for 0.6 * CLK_PER;
        TIMER_ERROR <= '0';        
        
        wait for 0.25 * CLK_PER;
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;
    
end Behavioral;
