----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2020 09:05:17
-- Design Name: 
-- Module Name: counter_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_tb is
--  Port ( );
end counter_tb;

architecture Behavioral of counter_tb is

    Component counter is
        Generic(
            FREQ : positive := 1000;
            MAX_TIME : positive := 5;
            SIZE_TRIGGER : positive := 3;
            TRIGGER_VALUE : STD_LOGIC_VECTOR (3 DOWNTO 0) := "1000"
        );    
		Port(		    
            CLK : in STD_LOGIC;
            RESET_N : in STD_LOGIC;
            TRIGGER : in STD_LOGIC_VECTOR(SIZE_TRIGGER DOWNTO 0);
            TIMER_FLAG : out STD_LOGIC
        );
	end Component;		
	
	--entradas
	signal CLK: std_logic; 
    signal RESET_N: std_logic;  
    signal TRIGGER: std_logic_vector(3 DOWNTO 0);
    
    --salidas
    signal TIMER_FLAG: std_logic; 
   
    --periodo del reloj
    constant CLK_OUT_PER : time := 1 ms;    
            
begin		
	uut: counter	
	   Generic map(
	        FREQ => 1000,
    		MAX_TIME => 10,
    		TRIGGER_VALUE => "1000"
    	)	      	
    	Port map(
    	    CLK => CLK,
            RESET_N => RESET_N,
            TRIGGER => TRIGGER,
            TIMER_FLAG => TIMER_FLAG
        );        
 
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_OUT_PER;
        CLK <= '1';
        wait for 0.5 * CLK_OUT_PER;
    end process;         
    
    RESET_N <= '0' after 0.25 * CLK_OUT_PER, '1' after 0.75 * CLK_OUT_PER;
    
    tester : process
    begin        
        wait until RESET_N = '1';
        wait for 250 ms;
        TRIGGER <= "1000"; --disparo
        wait for 10 sec;
        TRIGGER <= "0001"; --no hay disparo
        wait for 15 sec;
        TRIGGER <= "1000"; --disparo
        wait for 10 sec;
        TRIGGER <= "0001"; --no hay disparo    
        
        wait for 500 ms;
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;
end Behavioral;
