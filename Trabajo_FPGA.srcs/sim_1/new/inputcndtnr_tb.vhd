----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2021 16:03:49
-- Design Name: 
-- Module Name: inputcndtnr_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity inputcndtnr_tb is
--  Port ( );
end inputcndtnr_tb;

architecture Behavioral of inputcndtnr_tb is
    Component inputcndtnr is
        Port( 
            CLK : in std_logic;
            BOTON_IN : in std_logic;
            BOTON_OUT : out std_logic
        );
    end component;
    
    --entradas
    signal CLK: std_logic; 
    signal BOTON_IN: std_logic := '0';
    
    --salidas  
    signal BOTON_OUT: std_logic;
    
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000;
begin
    uut: inputcndtnr Port map(
        	CLK => CLK,
            BOTON_IN => BOTON_IN,
            BOTON_OUT => BOTON_OUT
        );
    clkgen : process
        begin
    	    CLK <= '0';
            wait for 0.5 * CLK_PER;
            CLK <= '1';
            wait for 0.5 * CLK_PER;
        end process; 
        
     tester : process
        begin        
            wait for 50 ms;
            BOTON_IN <= '1';
            wait for 5 ms; --pulso =< 20 ms -> no v�lido
            BOTON_IN <= '0';
            wait for 15 ms;
            BOTON_IN <= '1';
            wait for 20 ms; --pulso >= 20 ms -> v�lido
            BOTON_IN <= '0';    
            
            wait for 60 ms;
            assert false
        	   report "[SUCCESS]: simulation finished."
       		   severity failure; 
        end process;
    
end Behavioral;
