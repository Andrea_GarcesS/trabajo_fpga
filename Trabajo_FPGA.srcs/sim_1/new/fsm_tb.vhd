----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.12.2020 11:08:25
-- Design Name: 
-- Module Name: fsm_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_coin_tb is
--  Port ( );
end fsm_coin_tb;

architecture Behavioral of fsm_coin_tb is
    Component fsm_coin
        Generic(
            SIZE_IN : positive := 2;
            SIZE_OUT : positive := 3
        );
        Port(
            RESET_N : in std_logic;
            CLK : in std_logic;
            COIN : in std_logic_vector(SIZE_IN DOWNTO 0);
            TIMER_ERROR : in std_logic;
            TIMER_OK: in std_logic;
            LIGHT : out std_logic_vector(SIZE_OUT DOWNTO 0)
        );
    end Component;
    
    --entradas
    signal RESET_N : std_logic;
    signal CLK : std_logic;
    signal COIN : std_logic_vector(2 DOWNTO 0); 
    signal TIMER_ERROR : std_logic;
    signal TIMER_OK: std_logic;
    
    --salidas
    signal LIGHT : std_logic_vector(3 DOWNTO 0);
    
    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000; 
    
begin
     uut : fsm_coin 
        Generic map(
            SIZE_IN => 2,
            SIZE_OUT => 3
        )
        Port map(
            RESET_N => RESET_N,
            CLK => CLK,
            COIN => COIN,
            TIMER_ERROR => TIMER_ERROR,
            TIMER_OK => TIMER_OK,
            LIGHT => LIGHT
            ); 
    
    clock: process
    begin
        CLK <= '0';
        wait for 20 ns;
        CLK <= '1';
        wait for 20 ns;
        CLK <= '0';            
    end process;
    
    RESET_N <= '0' after 0.25 * CLK_PER, '1' after 0.75 * CLK_PER;
    
    item: process
    begin
        TIMER_OK <= '0';
        COIN <= "001"; --10c
        wait for 40 ns;
        COIN <= "011"; --50c
        wait for 40 ns;
        COIN <= "010"; --20c
        wait for 40 ns;
        COIN <= "010"; --20c
        TIMER_OK <= '1'; --finalización del temporizador timer_ok
        
        wait for 80 ns;  
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;

end Behavioral;
