----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.01.2021 08:31:35
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is

    component top is    
		Generic(
            SIZE_SW : positive := 3;
            SIZE_OUT : positive := 7
        );
        Port(
            BOTON_COIN : in std_logic;
            BOTON_PRODUCTO : in std_logic;
            SW_COIN : in std_logic_vector(SIZE_SW DOWNTO 0);
            SW_PRODUCTO : in std_logic_vector(SIZE_SW DOWNTO 0);
            RESET_N : in std_logic;
            CLK : in std_logic;
            AN : out std_logic_vector(SIZE_OUT DOWNTO 0); 
            LED: out std_logic_vector(SIZE_OUT DOWNTO 0) 
        );
	end component;
	
	--entradas
	signal BOTON_COIN: std_logic;
	signal BOTON_PRODUCTO: std_logic;
	signal SW_COIN: std_logic_vector(3 DOWNTO 0);
    signal SW_PRODUCTO: std_logic_vector(3 DOWNTO 0);
	signal RESET_N: std_logic;
    signal CLK: std_logic;
    
    --salidas 
    signal AN: std_logic_vector(7 DOWNTO 0);
    signal LED: std_logic_vector(7 DOWNTO 0);     

    --periodo del reloj
    constant CLK_PER : time := 1 sec / 100_000_000; 
        
begin		
	uut: top 
	    Generic map(
               SIZE_SW => 3,
               SIZE_OUT => 7
               )   	
    	Port map(
    	    BOTON_COIN => BOTON_COIN,
    	    BOTON_PRODUCTO => BOTON_PRODUCTO,
    	    SW_COIN => SW_COIN,
    	    SW_PRODUCTO => SW_PRODUCTO,
        	RESET_N => RESET_N,
            CLK => CLK,
            AN => AN,
            LED => LED
            );
        
	clkgen : process
    begin
    	CLK <= '0';
        wait for 0.5 * CLK_PER;
        CLK <= '1';
        wait for 0.5 * CLK_PER;
    end process;
    
    RESET_N <= '0' after 0.25 * CLK_PER, '1' after 0.75 * CLK_PER;
    
    tester : process
    begin        
    	wait until RESET_N = '1';   
        
        wait for 100 ms; 
        SW_COIN <= "0010"; --50c
        wait for 15 ms;
        BOTON_COIN <= '1';
        wait for 30 ms;
        BOTON_COIN <= '0';
        
        wait for 100 ms; 
        SW_COIN <= "0100"; --20c
        wait for 15 ms;
        BOTON_COIN <= '1';
        wait for 30 ms;
        BOTON_COIN <= '0';
        
        wait for 50 ms; 
        SW_COIN <= "0100"; --20c
        wait for 12 ms;
        BOTON_COIN <= '1';
        wait for 30 ms;
        BOTON_COIN <= '0';
        
        wait for 100 ms; 
        SW_COIN <= "1000"; --10c
        wait for 14 ms;
        BOTON_COIN <= '1';
        wait for 30 ms;
        BOTON_COIN <= '0';  
        
        wait for 20 ms;        
        SW_PRODUCTO <= "1000"; --PRODUCTO 1
        wait for 40 ms;
        BOTON_PRODUCTO <= '1';
        wait for 30 ms;
        BOTON_PRODUCTO <= '0';
        
        wait for 12 sec;
        assert false
        	report "[SUCCESS]: simulation finished."
       		severity failure;
    end process;

end Behavioral;