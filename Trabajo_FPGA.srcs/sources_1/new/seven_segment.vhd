----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2021 08:20:01
-- Design Name: 
-- Module Name: seven_segment - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity seven_segment is   
    Generic(
        SIZE_IN : positive := 3; --tama�o de COIN y PRODUCTO
        SIZE_OUT : positive := 7; --tama�o de AN y LED
        FREQ : positive := 200 --frecuencia de actualizaci�n de la se�al de selecci�n
    );
    Port(
        CLK : in std_logic; --reloj de 100 MHz
        RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
        COIN: in  std_logic_vector(SIZE_IN DOWNTO 0); --salida de fsm_coin
        PRODUCTO: in  std_logic_vector(SIZE_IN - 1 DOWNTO 0); --salida de fsm_producto
        AN : out std_logic_vector(SIZE_OUT DOWNTO 0); --selecci�n del display
        LED : out std_logic_vector(SIZE_OUT DOWNTO 0) --segmentos del display (7 segmentos y punto decimal)         
    );
end seven_segment;

architecture Behavioral of seven_segment is

    Component freq_divider
        Generic(
            FREQ : positive := 1000
        );
        Port(
            CLK : in std_logic;
            CLK_OUT : out std_logic
        );
    end Component;
    
    Component decoder_bcd
        generic(
            SIZE_IN : positive:= 4;
            SIZE_OUT : positive := 7
            );     
        PORT (
            CODE: in std_logic_vector(SIZE_IN DOWNTO 0);
            LED : out std_logic_vector(SIZE_OUT DOWNTO 0) 
        );
    end Component;
    
    Component seven_segment_mux
        Generic(
            SIZE_IN : positive:= 4;
            SIZE_AN : positive:= 7;
            SIZE_CODE : positive:= 4
            );  
        Port(
            CLK : in std_logic;
            RESET_N : in std_logic;
            N_0 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_1 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_2 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_3 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_4 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_5 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_6 : in std_logic_vector(SIZE_IN DOWNTO 0);
            N_7 : in std_logic_vector(SIZE_IN DOWNTO 0);
            AN : out std_logic_vector(SIZE_AN DOWNTO 0);
            CODE : out std_logic_vector(SIZE_CODE DOWNTO 0)
        );     
    end Component;
    
    Component DECODER_COIN_SEVEN_SEGMENT
        Generic(
            SIZE_IN : positive:= 3;
            SIZE_OUT : positive:= 4
        );
        Port(
            COIN : in std_logic_vector(SIZE_IN DOWNTO 0);       
            N_0 : out std_logic_vector(SIZE_OUT DOWNTO 0); 
            N_1 : out std_logic_vector(SIZE_OUT DOWNTO 0);
            N_2 : out std_logic_vector(SIZE_OUT DOWNTO 0);
            N_3 : out std_logic_vector(SIZE_OUT DOWNTO 0)       
        );     
    end Component;
    
    Component DECODER_PRODUCTO_SEVEN_SEGMENT
        Generic(
            SIZE_IN : positive:= 2;
            SIZE_OUT : positive:= 4
        );
        Port ( 
            PRODUCTO : IN std_logic_vector(SIZE_IN DOWNTO 0);
            N_4 : out std_logic_vector(SIZE_OUT DOWNTO 0); 
            N_5 : out std_logic_vector(SIZE_OUT DOWNTO 0);
            N_6 : out std_logic_vector(SIZE_OUT DOWNTO 0);
            N_7 : out std_logic_vector(SIZE_OUT DOWNTO 0)       
        );     
    end Component;
        
    signal clk_out_s : std_logic;
    signal code_s : std_logic_vector(SIZE_IN + 1 DOWNTO 0);
    signal n0_s, n1_s, n2_s, n3_s, n4_s, n5_s, n6_s, n7_s: std_logic_vector(SIZE_IN + 1 DOWNTO 0);
    
begin
    Inst_freq_divider : freq_divider --divisior de frecuencia para obtener reloj de menor frecuencia 
        Generic map(
            FREQ => FREQ   
            )
        Port map(
            CLK => CLK,
            CLK_OUT => clk_out_s
            );    

    Inst_decoder_bcd : decoder_bcd --decodificador para la correspondencia de la iluminaci�n de segmentos con el caracter introducido 
        Generic map(
            SIZE_IN => SIZE_IN + 1,
            SIZE_OUT => SIZE_OUT
            )   
        Port map(
            CODE => code_s,
            LED => LED
            );
        
    Inst_seven_segment_mux : seven_segment_mux --actualizaci�n de la se�al de selecci�n con cada flanco de reloj     
        Generic map (
                SIZE_IN => SIZE_IN + 1,
                SIZE_AN => SIZE_OUT,
                SIZE_CODE => SIZE_IN + 1
                )
        Port map (
            CLK => clk_out_s,
            RESET_N => RESET_N,
            N_0 => n0_s,
            N_1 => n1_s,
            N_2 => n2_s,
            N_3 => n3_s,
            N_4 => n4_s,
            N_5 => n5_s,
            N_6 => n6_s,
            N_7 => n7_s,
            AN => AN,
            CODE => code_s
            );
        
    Inst_decoder_coin_seven_segment: DECODER_COIN_SEVEN_SEGMENT --asignaci�n de la informaci�n a mostrar en los 4 displays de la derecha a partir de la se�al de salida de fsm_coin 
        Generic map(
             SIZE_IN => SIZE_IN,
             SIZE_OUT => SIZE_OUT - 3
             )
        Port map(
            COIN => seven_segment.COIN,
            N_0 => n0_s,
            N_1 => n1_s,
            N_2 => n2_s,
            N_3 => n3_s
            );
    
    Inst_decoder_producto_seven_segment: DECODER_PRODUCTO_SEVEN_SEGMENT --asignaci�n de la informaci�n a mostrar en los 4 displays de la izquierda a partir de la se�al de salida de fsm_producto
        Generic map(
             SIZE_IN => SIZE_IN - 1,
             SIZE_OUT => SIZE_OUT - 3
             )
        Port map(
            PRODUCTO => seven_segment.PRODUCTO,
            N_4 => n4_s,
            N_5 => n5_s,
            N_6 => n6_s,
            N_7 => n7_s
            );    

end Behavioral;