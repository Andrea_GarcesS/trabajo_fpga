----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2020 12:18:53
-- Design Name: 
-- Module Name: decoder_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity encoder_coin is
  Generic(
        SIZE_IN : positive := 3; --tama�o de SW
        SIZE_OUT : positive := 2 --tama�o de COIN
  );
  Port(
        BUTTON : in std_logic; --bot�n para introducir moneda
        SW : in std_logic_vector(SIZE_IN DOWNTO 0); --SW para seleccionar moneda
        COIN : out std_logic_vector(SIZE_OUT DOWNTO 0) --moneda codificada
   );
end encoder_coin;

architecture Behavioral of encoder_coin is
begin
    process(SW, BUTTON)
    begin
        if BUTTON = '1' then --durante un pulso del bot�n
            case SW is
                when "1000" => --SW 3                            
                    COIN <= "001"; --10c                    
                when "0100" => --SW 2                   
                    COIN <= "010"; --20c                   
                when "0010" => --SW 1                   
                    COIN <= "011"; --50c                    
                when "0001" => --SW 0                    
                    COIN <= "100"; --1�                    
                when others => --cualquier otro caso                    
                    COIN <= (others => '0'); --0c                    
            end case;
        else --si no hay pulso
            COIN <= (others => '0'); --0c
        end if;
    end process; 
                   
end Behavioral;