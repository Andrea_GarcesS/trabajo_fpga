----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2021 14:58:12
-- Design Name: 
-- Module Name: decoder_producto_trigger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_producto_trigger is
    port (
        PRODUCTO_OUT : in std_logic_vector(3 downto 0);
        TRIGGER_IN : out std_logic_vector(3 downto 0)               
    );
end decoder_producto_trigger;

architecture Behavioral of decoder_producto_trigger is
begin
    TRIGGER_IN <= "0000" when PRODUCTO_OUT = "0000"
                  else "0001";
end Behavioral;
