----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2020 09:36:53
-- Design Name: 
-- Module Name: decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_bcd IS 
    Generic(
        SIZE_IN : positive:= 4; --tama�o de CODE
        SIZE_OUT : positive := 7 --tama�o de LED
    );
    
    Port (
        CODE : in std_logic_vector(SIZE_IN  DOWNTO 0); --caracter introducido (bits [4-1] -> caracter, bit 0 -> punto decimal (1 -> hay punto, 0 -> no hay punto)        
        LED : out std_logic_vector(SIZE_OUT DOWNTO 0) --segmentos correspondientes al caracter (bits [7-1] -> 7 segmentos (a, b, c, ..., f, g), bit 0 -> pd (punto decimal))        
    );
end decoder_bcd;

architecture dataflow of decoder_bcd is
    signal led_s: std_logic_vector(SIZE_OUT  DOWNTO 0);
    signal point: std_logic; --punto decimal del display
    signal code_s: std_logic_vector(SIZE_IN - 1 DOWNTO 0);
    
begin
    code_s <= CODE(SIZE_IN DOWNTO 1);  --para el with select se omite el bit de punto decimal

    with code_s select --por defecto el punto decimal est� apagado
        led_s <= "00000011" when "0000", --0
               "10011111" when "0001", --1
               "00100101" when "0010", --2 
               "00001101" when "0011", --3 
               "10011001" when "0100", --4 
               "01001001" when "0101", --5
               "01000001" when "0110", --6 
               "00011111" when "0111", --7 
               "00000001" when "1000", --8
               "00001001" when "1001", --9
               "00110001" when "1011", --P
               "11111111" when "1100", -- ' '
               "01110001" when "1101",--F
               "00010001" when "1110",--A
               "11110011" when "1010", --I
               "11100011" when "1111",--L
               "11111101" when others; -- -
               
    point <= CODE(0); --se obtiene el bit de punto decimal
               
    with point select
        LED <= led_s(SIZE_OUT DOWNTO 1) & '0' when '1', --si hay punto, se activa el led del punto
               led_s when others; --si el bit est� a 0 se deja como estaba
               
end dataflow;
