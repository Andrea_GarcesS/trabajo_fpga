----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.12.2020 11:57:18
-- Design Name: 
-- Module Name: debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Alberto Garc�a Serrano. DIGILOGIC (https://www.digilogic.es/pulsador-antirrebote-en-vhdl-debouncing/)
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
    Port(
        CLK : in std_logic; --input clock      
        BUTTON : in  std_logic; --input signal to be debounced
        RESULT : out std_logic --debounced signal
        ); 
end debouncer;

architecture Behavioral of debouncer is
 
    constant counter_size : integer := 20; --counter bit size (20 ms for stable value)
    signal btn_prev : std_logic := '0'; --flipflop that stores previous state
    signal counter : std_logic_vector(counter_size DOWNTO 0) := (others => '0'); --counter for timing
     
begin

    debounce: process(clk)
    begin
	if (clk'event and clk = '1') then --rising clock edge
		if (btn_prev xor BUTTON) = '1' then --compares if previous state and current state are different
			counter <= (others => '0'); --reset counter because input is changing 
			btn_prev <= BUTTON; --store button value in flipflop
		elsif (counter(counter_size) = '0') then --if previous state and current state are equal begins counter
			counter <= counter + 1; -- use IEEE.STD_LOGIC_UNSIGNED.ALL for opreation
        	else
			RESULT <= btn_prev; --output the stable value
		end if;
	end if;
	
end process;  
end Behavioral;

