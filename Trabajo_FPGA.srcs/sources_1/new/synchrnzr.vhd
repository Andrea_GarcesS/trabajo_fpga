----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.12.2020 12:35:12
-- Design Name: 
-- Module Name: synchrnzr - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity synchrnzr is
  Port(
    CLK : in std_logic; --reloj de 100 MHz
    ASYNC_IN : in std_logic; --se�al del bot�n as�ncrona
    SYNC_OUT : out std_logic --se�al del bot�n sincronizada
  );
end synchrnzr;

architecture Behavioral of synchrnzr is
    signal sreg : std_logic_vector (1 DOWNTO 0);
begin
    process(CLK)
    begin
        if rising_edge(CLK) then --cuando hay flanco de subida del reloj
            SYNC_OUT <= sreg(1); --se asigna la posici�n 1 de sreg a la salida SYNC_OUT
            sreg <= sreg(0) & ASYNC_IN; --desplazamiento hacia la izquierda para recibir nuevo ASYNC_IN en la posici�n 0
        end if;
    end process;            
end Behavioral;