----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.12.2020 21:11:58
-- Design Name: 
-- Module Name: encoder_producto - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity encoder_producto is
    Generic(
        SIZE_IN : positive := 3; --tama�o de SW
        SIZE_OUT : positive := 2 --tama�o de PRODUCTO
    );
    Port(
         BUTTON : in std_logic; --bot�n para confirmar producto
         SW : in std_logic_vector(SIZE_IN DOWNTO 0); --SW para seleccionar producto
         PRODUCTO : out std_logic_vector(SIZE_OUT DOWNTO 0) --producto codificado
     );
end encoder_producto;

architecture Behavioral of encoder_producto is
begin
    process(SW, BUTTON)
    begin
        if BUTTON = '1' then --durante un pulso del bot�n
            case SW is
                when "1000" => --SW 15                           
                    PRODUCTO <= "001"; --PRODUCTO 1                    
                when "0100" => --SW 14                   
                    PRODUCTO <= "010"; --PRODUCTO 2                   
                when "0010" => --SW 13                    
                    PRODUCTO <= "011"; --PRODUCTO 3                    
                when "0001" => --SW 12                    
                    PRODUCTO <= "100"; --PRODUCTO 4                    
                when others => --cualquier otro caso                    
                    PRODUCTO <= (others => '0'); --PRODUCTO 0 (producto sin seleccionar)                    
            end case;
        else --si no hay pulso
            PRODUCTO <= (others => '0'); --PRODUCTO 0 (producto sin seleccionar)
        end if;
    end process; 
                   
end Behavioral;