----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2020 09:47:31
-- Design Name: 
-- Module Name: timer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity timer is
  Generic(
    FREQ : positive := 1000; --frecuencia para el contador (error m�ximo(s) = 1 / FREQ)
    MAX_TIME : positive := 10; --tiempo a contar en segundos
    SIZE_TRIGGER : positive := 3; --tama�o de TRIGGER
    TRIGGER_VALUE : std_logic_vector(3 DOWNTO 0) := "1000" --valor de TRIGGER que dispara el temporizador
  );
  Port(
    CLK : in std_logic; --reloj de 100 MHz
    RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
    TRIGGER : in std_logic_vector(SIZE_TRIGGER DOWNTO 0); --se�al de disparo del temporizador
    TIMER_FLAG : out std_logic --se�al de finalizaci�n del temporizador
   );
end timer;

architecture Behavioral of timer is

    Component freq_divider
        Generic(
            FREQ : positive := 1000
        );
        Port(
            CLK : in std_logic;
            CLK_OUT : out std_logic
        );
    end Component; 
    
    Component counter
        Generic(
            FREQ : positive := 1000;
            MAX_TIME : positive := 10;
            SIZE_TRIGGER : positive := 3;
            TRIGGER_VALUE : std_logic_vector(3 DOWNTO 0) := "1000"
        );
        Port(
            CLK : in std_logic;
            RESET_N : in std_logic;
            TRIGGER : in std_logic_vector(SIZE_TRIGGER DOWNTO 0);
            TIMER_FLAG : out std_logic
        );
    end Component;    
    
    signal clk_out_s : std_logic;
    
begin
    Inst_freq_divider : freq_divider --divisior de frecuencia para obtener reloj de menor frecuencia
        Generic map(
            FREQ => FREQ            
             )
        Port map(
            CLK => CLK,
            CLK_OUT => clk_out_s
            );
    
    Inst_counter : counter --contador de flancos de subida del reloj y transformaci�n a segundos
        Generic map(
            FREQ => FREQ,
            MAX_TIME => MAX_TIME,
            SIZE_TRIGGER => SIZE_TRIGGER,
            TRIGGER_VALUE => TRIGGER_VALUE
            )
        Port map(        
            CLK => clk_out_s,
            RESET_N => RESET_N,
            TRIGGER => TRIGGER,
            TIMER_FLAG => TIMER_FLAG
            );  

end Behavioral;
