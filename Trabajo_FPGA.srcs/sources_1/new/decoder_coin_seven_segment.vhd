----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2021 12:01:02
-- Design Name: 
-- Module Name: decoder_coin_seven_segment - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_coin_seven_segment is
    Generic(
        SIZE_IN : positive:= 3; --tama�o de COIN
        SIZE_OUT : positive:= 4 --tama�o del caracter N_x
    );
    Port(
        COIN : in std_logic_vector(SIZE_IN DOWNTO 0); --salida de fsm_coin
        N_0 : out std_logic_vector(SIZE_OUT DOWNTO 0); --caracter del display 0 
        N_1 : out std_logic_vector(SIZE_OUT DOWNTO 0); --caracter del display 1
        N_2 : out std_logic_vector(SIZE_OUT DOWNTO 0); --caracter del display 2
        N_3 : out std_logic_vector(SIZE_OUT DOWNTO 0) --caracter del display 3       
    );
end decoder_coin_seven_segment;

architecture Behavioral of decoder_coin_seven_segment is
begin 
    process(COIN)
    begin
        case COIN is
            when "0000" => --S0 (00.00)  
                N_3 <= (others => '0'); --0             
                N_2 <= "00001"; --0.
                N_1 <= "00000"; --0
                N_0 <= (others => '0'); --0
            when "0001" => --S1 (00.10)
                N_3 <= (others => '0'); --0 
                N_2 <= "00001"; --0.
                N_1 <= "00010"; --1
                N_0 <= (others => '0'); --0
            when "0010" => --S2 (00.20)                
                N_3 <= (others => '0'); --0 
                N_2 <= "00001"; --0.
                N_1 <= "00100"; --2 
                N_0 <= (others => '0'); --0              
            when "0011" => --S3 (00.30)             
                N_3 <= (others => '0'); --0
                N_2 <= "00001"; --0.
                N_1 <= "00110"; --3 
                N_0 <= (others => '0'); --0              
            when "0100" => --S4 (00.40)
                N_3 <= (others => '0'); --0
                N_2 <= "00001"; --0.
                N_1 <= "01000"; --4
                N_0 <= (others => '0'); --0                  
            when "0101" => --S5 (00.50)
                N_3 <= (others => '0'); --0
                N_2 <= "00001"; --0.
                N_1 <= "01010"; --5
                N_0 <= (others => '0'); --0              
            when "0110" => --S6 (00.60)              
                N_3 <= (others => '0'); --0
                N_2 <= "00001"; --0.
                N_1 <= "01100"; --6 
                N_0 <= (others => '0'); --0             
            when "0111" => --S7 (00.70)
                N_3 <= (others => '0'); --0  
                N_2 <= "00001"; --0.
                N_1 <= "01110"; --7 
                N_0 <= (others => '0'); --0             
            when "1000" => --S8 (00.80)               
                N_3 <= (others => '0'); --0 
                N_2 <= "00001"; --0.
                N_1 <= "10000"; --8  
                N_0 <= (others => '0'); --0          
            when "1001" => --S9 (00.90)              
                N_3 <= (others => '0'); --0 
                N_2 <= "00001"; --0.
                N_1 <= "10010"; --9
                N_0 <= (others => '0'); --0
            when "1010" => --S10 (01.00)               
                N_3 <= (others => '0'); --0 
                N_2 <= "00011"; --1.
                N_1 <= "00000"; --0  
                N_0 <= (others => '0'); --0                                            
            when "1011" => --S11 (FAIL)
                N_3 <= "11010"; --F
                N_2 <= "11100"; --A
                N_1 <= "10100"; --I
                N_0 <= "11110"; --L
            when others => --cualquier otro caso
                N_3 <= "11000"; -- ' '
                N_2 <= "11000"; -- ' '
                N_1 <= "11000"; -- ' '
                N_0 <= "11000"; -- ' '
        end case;
end process;

end Behavioral;