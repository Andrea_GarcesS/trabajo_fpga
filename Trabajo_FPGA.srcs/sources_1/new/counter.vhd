----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2020 08:24:03
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
  Generic (
    FREQ : positive := 1000; --frecuencia del reloj
    MAX_TIME : positive := 10; --tiempo a contar en segundos
    SIZE_TRIGGER : positive := 3; --tama�o de TRIGGER
    TRIGGER_VALUE : std_logic_vector (3 DOWNTO 0) := "1000" --valor de TRIGGER que dispara el temporizador
  );
  
  Port (
    CLK : in std_logic; --reloj de FREQ
    RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
    TRIGGER : in std_logic_vector (SIZE_TRIGGER DOWNTO 0); --se�al de disparo del contador
    TIMER_FLAG : out std_logic --se�al de finalizaci�n del contador
  );
end counter;

architecture Behavioral of counter is
    signal count : unsigned(16 DOWNTO 0); --MAX_TIME * FREQ <= 2^16 - 1 = 65535
    signal flag : std_logic; --bandera de disparo recibido
begin
    process(CLK, RESET_N, TRIGGER)
    begin
        if RESET_N = '0' then --si hay reset
            count <= (others => '0'); --reinicio de la cuenta
            TIMER_FLAG <= '0'; --se�al de finalizaci�n a 0
        elsif rising_edge(CLK) then --si hay flanco de subida del reloj
            if TRIGGER = TRIGGER_VALUE then --si se recibe el disparo
                flag <= '1'; --bandera a 1
            end if;            
            if flag = '1' then --si la bandera est� activa
                count <= count + 1; --se realiza la cuenta
            end if;            
            if count = (MAX_TIME * FREQ) - 1 then --si la cuenta acaba
                TIMER_FLAG <= '1'; --se asigna 1 a la se�al de finalizaci�n del contador (pulso de duraci�n del periodo del reloj)
                flag <= '0'; --bandera a 0
                count <= (others => '0'); --reinicio de la cuenta
            else --si no ha acabado a�n
                TIMER_FLAG <= '0'; --se�al de finalizaci�n a 0      
            end if;            
        end if;
    end process;        
end Behavioral;
