----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.12.2020 10:21:21
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
  Generic(
         SIZE_SW : positive := 3; --tama�o de los dos sets de SW
         SIZE_OUT : positive := 7 --tama�o de la se�al de control y de los segmentos de los displays
  );
  Port(
         BOTON_COIN : in std_logic; --bot�n para introducir moneda
         BOTON_PRODUCTO : in std_logic; --bot�n para confirmar producto
         SW_COIN : in std_logic_vector(SIZE_SW DOWNTO 0); --SW para seleccionar moneda
         SW_PRODUCTO : in std_logic_vector(SIZE_SW DOWNTO 0); --SW para seleccionar producto
         RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
         CLK : in std_logic; --reloj de 100 MHz
         AN : out std_logic_vector(SIZE_OUT DOWNTO 0); --selecci�n de display de 7 segmentos
         LED: out std_logic_vector(SIZE_OUT DOWNTO 0) --segmentos del display de 7 segmentos
  );
end top;

architecture Behavioral of top is     
    Component ENCODER_COIN
        Generic(
            SIZE_IN : positive := 3;
            SIZE_OUT : positive := 2
        );
        Port(
            BUTTON : in std_logic;
            SW : in std_logic_vector(SIZE_IN DOWNTO 0);
            COIN : out std_logic_vector(SIZE_OUT DOWNTO 0)
        );
    end Component;
    
    Component FSM_COIN
        Generic(
            SIZE_IN : positive := 2;
            SIZE_OUT : positive := 2
        );
        Port (
            RESET_N : in std_logic;
            CLK : in std_logic;
            COIN : in std_logic_vector(SIZE_IN DOWNTO 0);
            TIMER_ERROR : in std_logic;
            TIMER_OK: in std_logic;
            LIGHT : out std_logic_vector(SIZE_OUT DOWNTO 0)
         );
    end Component;
    
    Component ENCODER_PRODUCTO 
        Generic(
            SIZE_IN : positive := 3;
            SIZE_OUT : positive := 2
        );
        Port (
            BUTTON : in std_logic;
            SW : in std_logic_vector(SIZE_IN DOWNTO 0);
            PRODUCTO : out std_logic_vector(SIZE_OUT DOWNTO 0)   
        );
    end Component;
    
    Component FSM_PRODUCTO
        Generic(
            SIZE_PRODUCTO : positive := 2;
            SIZE_SELECT : positive := 3;
            SIZE_LIGHT : positive := 2;
            SELECT_START_VALUE : std_logic_vector(3 DOWNTO 0) := "1010"
        );
        Port (
            RESET_N : in std_logic;
            CLK : in std_logic;
            PRODUCTO : in std_logic_vector(SIZE_PRODUCTO DOWNTO 0); 
            SELECT_START : in std_logic_vector(SIZE_SELECT DOWNTO 0);
            TIMER_OK : in std_logic;
            TIMER_ERROR : in std_logic;        
            LIGHT : out std_logic_vector(SIZE_LIGHT DOWNTO 0)
        );
    end Component;
    
    Component INPUTCNDTNR 
        Port ( 
            CLK : in std_logic;
            BOTON_IN : in std_logic;
            BOTON_OUT : out std_logic
            );
    end Component;
    
    Component TIMER
        Generic (
            FREQ : positive := 1000;
            MAX_TIME : positive := 10;
            SIZE_TRIGGER : positive := 3;
            TRIGGER_VALUE : std_logic_vector(3 DOWNTO 0) := "1000"
            );
        Port (
            CLK : in std_logic;
            RESET_N : in std_logic;
            TRIGGER : in std_logic_vector(SIZE_TRIGGER DOWNTO 0);
            TIMER_FLAG : out std_logic
            );     
    end Component;
    
    Component ENCODER_PRODUCTO_TRIGGER
        Generic(
            SIZE_IN : positive := 2;
            SIZE_OUT : positive := 3
         );
        Port (
            PRODUCTO_OUT : in std_logic_vector(SIZE_IN DOWNTO 0);
            TRIGGER_IN : out std_logic_vector(SIZE_OUT DOWNTO 0)               
        );
    end Component;
    
    Component SEVEN_SEGMENT
        Generic(
            SIZE_IN : positive := 3;
            SIZE_OUT : positive := 7;
            FREQ : positive := 200
        );
        Port(
            CLK : in std_logic; 
            RESET_N : in std_logic;
            COIN: in  std_logic_vector(SIZE_IN  DOWNTO 0); 
            PRODUCTO: in  std_logic_vector(SIZE_IN - 1  DOWNTO 0); 
            AN : out std_logic_vector(SIZE_OUT  DOWNTO 0);
            LED : out std_logic_vector(SIZE_OUT  DOWNTO 0)       
        );     
    end Component;
    
    signal boton_coin_edge: std_logic;
    signal money: std_logic_vector(2 DOWNTO 0);   
    signal timer_ok: std_logic;
    signal timer_error: std_logic;
    
    signal trigger_timer_ok: std_logic_vector(3 DOWNTO 0);
    
    signal led_coin_s: std_logic_vector(3 DOWNTO 0);
    signal led_producto_s: std_logic_vector(2 DOWNTO 0);  
      
    signal boton_producto_edge: std_logic;
    signal item: std_logic_vector(2 DOWNTO 0);

begin
     Fsm_coin_inputcndtnr: INPUTCNDTNR Port map( --acondicionamiento de la se�al del bot�n BOTON_COIN
                        CLK => top.CLK,
                        BOTON_IN => BOTON_COIN,
                        BOTON_OUT => boton_coin_edge
                         );                         
     Fsm_coin_encoder_coin: ENCODER_COIN --codificaci�n de SW_COIN para fsm_coin
                            Generic map(
                                SIZE_IN => SIZE_SW,
                                SIZE_OUT => SIZE_SW - 1
                                )
                            Port map(
                                BUTTON => boton_coin_edge,
                                SW => SW_COIN,
                                COIN => money
                                );
     Fsm_coin_inst: FSM_COIN --m�quina de estados de las monedas
                    Generic map(
                           SIZE_IN => SIZE_SW - 1,
                           SIZE_OUT => SIZE_SW
                           ) 
                    Port map(
                        RESET_N => top.RESET_N,
                        CLK => top.CLK,
                        COIN => money,
                        TIMER_ERROR => timer_error,
                        TIMER_OK => timer_ok,
                        LIGHT => led_coin_s
                        );
     Fsm_timer_ok: TIMER --temporizador para simular la recepci�n de un producto
                    Generic map(
                        FREQ => 1000, --frecuencia para el contador (error m�ximo = 1 / 1000 = 1 ms)
                        MAX_TIME => 10, --tiempo a contar en segundos
                        SIZE_TRIGGER => SIZE_SW,
                        TRIGGER_VALUE => "0001" --valor de TRIGGER que dispara el temporizador (producto != 0)
                        )
                    Port map(
                        CLK => top.CLK,
                        RESET_N => top.RESET_N,
                        TRIGGER => trigger_timer_ok,
                        TIMER_FLAG => timer_ok
                        );            
            
     Fsm_timer_error: TIMER --temporizador para simular la devoluci�n del dinero en caso de exceso
                    Generic map(
                        FREQ => 1000, --frecuencia para el contador (error m�ximo = 1 / 1000 = 1 ms)
                        MAX_TIME => 15, --tiempo a contar en segundos
                        SIZE_TRIGGER => SIZE_SW,
                        TRIGGER_VALUE => "1011" --valor de TRIGGER que dispara el temporizador (S11 de fsm_coin)
                        )
                    Port map(
                        CLK => top.CLK,
                        RESET_N => top.RESET_N,
                        TRIGGER => led_coin_s,
                        TIMER_FLAG => timer_error
                        );
        
     Fsm_producto_inputcndtnr: INPUTCNDTNR Port map( --acondicionamiento de la se�al del bot�n BOTON_PRODUCTO
                            CLK => top.CLK,
                            BOTON_IN => BOTON_PRODUCTO,
                            BOTON_OUT => boton_producto_edge
                            ); 
     Fsm_producto_encoder_producto: ENCODER_PRODUCTO --codificaci�n de SW_PRODUCTO para fsm_producto
                           Generic map(
                                SIZE_IN => SIZE_SW,
                                SIZE_OUT => SIZE_SW - 1
                                )
                           Port map(
                                BUTTON => boton_producto_edge,
                                SW => SW_PRODUCTO,
                                PRODUCTO => item
                                );
     Fsm_producto_inst: FSM_PRODUCTO --m�quina de estados de los productos
                        Generic map(
                                SIZE_PRODUCTO => SIZE_SW - 1,
                                SIZE_SELECT => SIZE_SW,
                                SIZE_LIGHT => SIZE_SW - 1,
                                SELECT_START_VALUE => "1010" --se puede seleccionar producto cuando se tiene 1� en fsm_coin (S10)
                                )
                        Port map(
                             RESET_N => top.RESET_N,
                             CLK => top.CLK,
                             PRODUCTO => item,
                             SELECT_START => led_coin_s,
                             TIMER_ERROR => timer_error,
                             TIMER_OK => timer_ok,
                             LIGHT => led_producto_s
                             );
     
     Fsm_producto_encoder_producto_trigger: ENCODER_PRODUCTO_TRIGGER --codificaci�n de LED_PRODUCTO para TRIGGER de timer_ok 
                    Generic map(
                        SIZE_IN => SIZE_SW - 1,
                        SIZE_OUT => SIZE_SW
                        )
                    Port map(
                         PRODUCTO_OUT => led_producto_s,
                         TRIGGER_IN => trigger_timer_ok
                         );  
     
     Fsm_expendedora_seven_segment: SEVEN_SEGMENT --visualizaci�n de la informaci�n de los estados en displays de 7 segmentos
                   Generic map(
                           SIZE_IN => SIZE_SW,
                           SIZE_OUT => SIZE_OUT,
                           FREQ => 400 --frecuencia de actualizaci�n de la se�al de selecci�n
                           )
                   Port map(
                         CLK => top.CLK,
                         RESET_N => top.RESET_N,
                         COIN => led_coin_s,
                         PRODUCTO => led_producto_s,
                         AN => AN,        
                         LED => LED 
                         );           
end Behavioral;
