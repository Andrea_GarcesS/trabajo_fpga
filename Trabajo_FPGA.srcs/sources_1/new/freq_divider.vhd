----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.12.2020 17:05:43
-- Design Name: 
-- Module Name: freq_divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity freq_divider is
    Generic (
        FREQ : positive := 1000 --frecuencia deseada
    );
    Port (
        CLK : in std_logic; --reloj de 100 MHz
        CLK_OUT : out std_logic --reloj de FREQ
    );

end freq_divider;

architecture Behavioral of freq_divider is
    constant max_count: integer := (100_000_000 / (2 * FREQ)) - 1; --se cuenta hasta la mitad del periodo para realizar el flanco de bajada y el flanco de subida
	signal count: integer range 0 to max_count;
	signal clk_out_s: std_logic := '0'; 
	
begin
	process(CLK, clk_out_s, count)
	begin
		if rising_edge(CLK) then --si hay flanco de subida del reloj
			if count < max_count then --si la cuenta a�n no ha acabado
				count <= count + 1; --se sigue contando
			else --si la cuenta ha acabado
				clk_out_s <= not clk_out_s; --se realiza un flanco
				count <= 0; --se resetea la cuenta
			end if;
		end if;
	end process;
	
	CLK_OUT <= clk_out_s;
	
end Behavioral;
