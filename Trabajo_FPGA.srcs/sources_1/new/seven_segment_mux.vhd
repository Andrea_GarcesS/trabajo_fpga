----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2021 08:35:54
-- Design Name: 
-- Module Name: seven_segment_mux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity seven_segment_mux is  
    Generic(
        SIZE_IN : positive:= 4;
        SIZE_AN : positive:= 7;
        SIZE_CODE : positive:= 4
    );  
    Port(
        CLK : in std_logic; --reloj de FREQ
        RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
        N_0 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 0 |||| bits [4-1] -> caracter, bit 0 -> punto decimal (1 -> hay punto, 0 -> no hay punto) 
        N_1 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 1
        N_2 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 2
        N_3 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 3
        N_4 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 4
        N_5 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 5
        N_6 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 6
        N_7 : in std_logic_vector(SIZE_IN DOWNTO 0); --caracter del display 7
        AN : out std_logic_vector(SIZE_AN DOWNTO 0); --selecci�n del display
        CODE : out std_logic_vector(SIZE_CODE DOWNTO 0) --caracter que se escribe 
    );
end seven_segment_mux;

architecture Behavioral of seven_segment_mux is    
     signal count : unsigned(3 DOWNTO 0) := (others => '0'); --cuenta inicializada a 0 
begin    
    process(CLK, RESET_N)     
    begin
        if RESET_N = '0' then --si hay reset
            AN <= (others => '1'); --ning�n display seleccionado
            CODE <= (others => '0'); --se escribe el caracter 8
            count <= "0001"; --reinicio de la cuenta a 1                
        elsif rising_edge(CLK) then --si hay flanco de subida del reloj 
            if count < 8 then --si la cuenta a�n no ha terminado
                count <= count + 1; --se sigue contando
            else --si ha terminado
                count <= "0001"; --reinicio de la cuenta a 1
            end if;
                            
            if count = 1 then --si la cuenta es 1                 
                AN <= "11111110"; --se activa el display 0
                CODE <= N_0; --se escribe su caracter
            elsif count = 2 then --si la cuenta es 2                
                AN <= "11111101"; --se activa el display 1
                CODE <= N_1; --se escribe su caracter
            elsif count = 3 then --si la cuenta es 3                 
                AN <= "11111011"; --se activa el display 2 
                CODE <= N_2; --se escribe su caracter
            elsif count = 4 then --si la cuenta es 4               
                AN <= "11110111"; --se activa el display 3
                CODE <= N_3; --se escribe su caracter
            elsif count = 5 then --si la cuenta es 5                
                AN <= "11101111"; --se activa el display 4
                CODE <= N_4; --se escribe su caracter
            elsif count = 6 then --si la cuenta es 6                 
                AN <= "11011111"; --se activa el display 5
                CODE <= N_5; --se escribe su caracter
            elsif count = 7 then --si la cuenta es 7                
                AN <= "10111111"; --se activa el display 6
                CODE <= N_6; --se escribe su caracter  
            elsif count = 8 then --si la cuenta es 8                
                AN <= "01111111"; --se activa el display 7
                CODE <= N_7; --se escribe su caracter               
            end if;                    
        end if;    
    end process;
    
end Behavioral;