----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.12.2020 12:27:58
-- Design Name: 
-- Module Name: inputcndtnr - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity inputcndtnr is
     Port( 
         CLK : in std_logic; --reloj de 100 MHz
         BOTON_IN : in std_logic; --se�al de entrada del bot�n
         BOTON_OUT : out std_logic --se�al acondicionada del bot�n
     );
end inputcndtnr;

architecture Behavioral of inputcndtnr is
    Component SYNCHRNZR 
        Port( 
             CLK : in std_logic;
             ASYNC_IN : in std_logic;
             SYNC_OUT : out std_logic        
        );
    end Component;
    
    Component EDGEDTCTR
        Port( 
             CLK : in std_logic;
             SYNC_IN : in std_logic;
             EDGE : out std_logic    
        );
    end Component;
    
    Component DEBOUNCER
        Port(
            CLK     : in  std_logic;    
            BUTTON  : in  std_logic;  
            RESULT  : out std_logic  
        );
    end Component;
            
    signal boton_sinc: std_logic;
    signal boton: std_logic;
    
begin
    Inst_debouncer: DEBOUNCER Port map( --filtro antirrebotes
                    CLK => inputcndtnr.CLK,
                    BUTTON => BOTON_IN,  
                    RESULT => boton
                    );
    Inst_sincronizer: SYNCHRNZR Port map( --sincronizaci�n de la se�al con el reloj
                        CLK => inputcndtnr.CLK,
                        ASYNC_IN => boton,
                        SYNC_OUT => boton_sinc                      
                        );                
    Inst_edgectrl: EDGEDTCTR Port map( --detector de flanco
                    CLK => inputcndtnr.CLK,
                    SYNC_IN => boton_sinc,
                    EDGE => BOTON_OUT    
                    );
end Behavioral;
