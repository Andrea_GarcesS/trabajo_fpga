----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.12.2020 12:35:12
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_coin is
    Generic(
        SIZE_IN : positive := 2; --tama�o de COIN
        SIZE_OUT : positive := 3 --tama�o de LIGHT
    );
    Port(
        RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
        CLK : in std_logic; --reloj de 100 MHz
        COIN : in std_logic_vector(SIZE_IN DOWNTO 0); --moneda codificada
        TIMER_ERROR : in std_logic; --se�al de finalizaci�n del temporizador timer_error
        TIMER_OK: in std_logic; --se�al de finalizaci�n del temporizador timer_ok
        LIGHT : out std_logic_vector(SIZE_OUT DOWNTO 0) --LEDs de salida de cada estado
    );
end fsm_coin;

architecture behavioral of fsm_coin is
    type STATES is (S0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11); --estados
    signal current_state: STATES; --estado actual
    signal next_state: STATES; --estado siguiente
begin
    state_register: process (RESET_N, CLK) --evoluci�n de estado con la se�al de reloj
    begin
        if RESET_N = '0' then --si hay reset
            current_state <= S0; --0c
        elsif rising_edge(CLK) then --cuando hay flanco de subida del reloj
            current_state <= next_state; --se evoluciona al siguiente estado
        end if;
    end process;
    
    nextstate_decod: process (COIN, current_state, TIMER_ERROR, TIMER_OK) --transici�n entre estados
    begin
        next_state <= current_state; --si no hay asignaci�n en case, se mantiene el estado actual
        case current_state is
            when S0 => --0c
                if COIN = "001" then --10c
                    next_state <= S1; --10c
                elsif COIN = "010" then --20c
                    next_state <= S2; --20c
                elsif COIN = "011" then --50c
                    next_state <= S5; --50c
                elsif COIN = "100" then --1�
                    next_state <= S10; --1�
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                end if;                
            when S1 => --10c
                if COIN = "001" then --10c
                    next_state <= S2; --20c
                elsif COIN = "010" then --20c
                    next_state <= S3; --30c
                elsif COIN = "011" then --50c
                    next_state <= S6; --60c
                elsif COIN = "100" then --1�
                    next_state <= S11; --1.10� (error)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                end if;
            when S2 => --20c
                if COIN = "001" then --10c
                    next_state <= S3; --30c
                elsif COIN = "010" then --20c
                    next_state <= S4; --40c
                elsif COIN = "011" then --50c
                    next_state <= S7; --70c
                elsif COIN = "100" then --1�
                    next_state <= S11; --1.20� (error)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                end if;
            when S3 => --30c
                if COIN = "001" then --10c
                    next_state <= S4; --40c
                elsif COIN = "010" then --20c
                    next_state <= S5; --50c
                elsif COIN = "011" then --50c
                    next_state <= S8; --80c
                elsif COIN = "100" then --1�
                    next_state <= S11; --1.30� (error)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                end if;
            when S4 => --40c
                if COIN = "001" then --10c
                    next_state <= S5; --50c
                elsif COIN = "010" then --20c
                    next_state <= S6; --60c
                elsif COIN = "011" then --50c
                    next_state <= S9; --90c
                elsif COIN = "100" then --1�
                    next_state <= S11; --1.40� (error)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                end if;
            when S5 => --50c
                if COIN = "001" then --10c
                    next_state <= S6; --60c
                 elsif COIN = "010" then --20c
                    next_state <= S7; --70c
                elsif COIN = "011" then --50c
                    next_state <= S10; --1� (ok)
                elsif COIN = "100" then --1�
                    next_state <= S11; --1.50� (error)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                end if;                
            when S6 => --60c
                if COIN = "001" then --10c
                    next_state <= S7; --70c
                elsif COIN = "010" then --20c
                    next_state <= S8; --80c
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                else --cualquier otro caso
                    next_state <= S11; --m�s de 1� (error)                
                end if;
            when S7 => --70c
                if COIN = "001" then --10c
                    next_state <= S8; --80c
                elsif COIN = "010" then --20c
                    next_state <= S9; --90c
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                else --cualquier otro caso
                    next_state <= S11; --m�s de 1� (error)
                end if;
            when S8 => --80c
                if COIN = "001" then --10c
                    next_state <= S9; --90c
                elsif COIN = "010" then --20c
                    next_state <= S10; --1� (ok)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                else --cualquier otro caso
                    next_state <= S11; --m�s de 1� (error)
                end if;
            when S9 => --90c
                if COIN = "001" then --10c
                    next_state <= S10; --1� (ok)
                elsif COIN = "000" then --0c
                    next_state <= current_state; --se mantiene el estado actual
                else --cualquier otro caso
                    next_state <= S11; --m�s de 1� (error) 
                end if;
            when S10 => --1� (ok)
                if TIMER_OK = '1' then --si el temporizador timer_ok ha finalizado
                    next_state <= S0; --0c
                end if;           
            when S11 => --m�s de 1� (error)
                if TIMER_ERROR = '1' then --si el temporizador timer_error ha finalizado
                    next_state <= S0; --0c   
                end if;        
            when others => --cualquier otro caso
                next_state <= S0; --0c
        end case;
    end process;
    
    output_decod: process (current_state) --salida asociada a los estados
    begin
        LIGHT <= (others => '0'); --si no hay asignaci�n en case, todos los LEDs apagados 
        case current_state is
            when S0 => --0c
                LIGHT <= "0000"; --LEDs apagados
            when S1 => --10c
                LIGHT <= "0001"; --LEDs          
            when S2 =>
                LIGHT <= "0010"; --LEDs 
            when S3 => --30c
                LIGHT <= "0011"; --LEDs 
            when S4 => --40c
                LIGHT <= "0100"; --LEDs         
            when S5 => --50c
                LIGHT <= "0101"; --LEDs 
            when S6 => --60c
                LIGHT <= "0110"; --LEDs 
            when S7 => --70c
                LIGHT <= "0111"; --LEDs           
            when S8 => --80c
                LIGHT <= "1000"; --LED 
            when S9 => --90c
                LIGHT <= "1001"; --LEDs 
            when S10 => --1� (ok)
                LIGHT <= "1010"; --LEDs 
             when S11 => --m�s de 1� (error)
                LIGHT <= "1011"; --LEDs           
            when others => --cualquier otro caso
                LIGHT <= (others => '0'); --LEDs apagados
        end case;
    end process;
end behavioral; 