----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.12.2020 12:01:16
-- Design Name: 
-- Module Name: fsm_producto - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_producto is
    Generic(
         SIZE_PRODUCTO : positive := 2; --tama�o de PRODUCTO
         SIZE_SELECT : positive := 3; --tama�o de SELECT_START
         SIZE_LIGHT : positive := 2; --tama�o de LIGHT
         SELECT_START_VALUE : std_logic_vector(3 DOWNTO 0) := "1010" --valor de SELECT_START que permite seleccionar producto
    );
    Port(
        RESET_N : in std_logic; --reset as�ncrono activo a nivel bajo
        CLK : in std_logic; --reloj de 100 MHz
        PRODUCTO : in std_logic_vector(SIZE_PRODUCTO DOWNTO 0); --producto codificado 
        SELECT_START : in std_logic_vector(SIZE_SELECT DOWNTO 0); --se�al de selecci�n de producto
        TIMER_OK : in std_logic; --se�al de finalizaci�n del temporizador timer_ok
        TIMER_ERROR : in std_logic; --se�al de finalizaci�n del temporizador timer_error        
        LIGHT : out std_logic_vector(SIZE_LIGHT DOWNTO 0) --LEDs de salida de cada estado
    );
end fsm_producto;

architecture behavioral of fsm_producto is
    type STATES is (S0, S1, S2, S3, S4); --estados
    signal current_state: STATES; --estado actual
    signal next_state: STATES; --estado siguiente
    
begin
    state_register: process (RESET_N, CLK) --evoluci�n de estado con la se�al de reloj
    begin
        if RESET_N = '0' then --si hay reset
            current_state <= S0; --PRODUCTO 0 (producto sin seleccionar)
        elsif rising_edge(CLK) then --si hay flanco de subida del reloj
            current_state <= next_state; --se evoluciona al siguiente estado
        end if;
    end process;
    
    nextstate_decod: process (PRODUCTO, SELECT_START, TIMER_OK, TIMER_ERROR, current_state) --transici�n entre estados
    begin
        next_state <= current_state; --si no hay asignaci�n en case, se mantiene el estado actual
        case current_state is
            when S0 => --PRODUCTO 0 (producto sin seleccionar)
                if SELECT_START = SELECT_START_VALUE then --si SELECT_START es SELECT_START_VALUE, se puede seleccionar el producto
                    if PRODUCTO = "001" then --PRODUCTO 1
                        next_state <= S1; --PRODUCTO 1
                    elsif PRODUCTO = "010" then --PRODUCTO 2
                        next_state <= S2; --PRODUCTO 2
                    elsif PRODUCTO = "011" then --PRODUCTO 3
                        next_state <= S3; --PRODUCTO 3
                    elsif PRODUCTO = "100" then --PRODUCTO 4
                        next_state <= S4; --PRODUCTO 4
                    elsif PRODUCTO = "000" then --PRODUCTO 0 (producto sin seleccionar)
                        next_state <= current_state; --se mantiene el estado actual                         
                    end if;
                end if;                
            when S1 | S2 | S3 | S4 => --habiendo elegido cualquiera de los productos
                if TIMER_OK = '1' or TIMER_ERROR = '1' then --si ha terminado cualquiera de los temporizadores
                    next_state <= S0; --PRODUCTO 0 (producto sin seleccionar)               
                end if;                    
            when others => --cualquier otro caso
                next_state <= S0; --PRODUCTO 0 (producto sin seleccionar)
        end case;
    end process;
    
    output_decod: process (current_state) --salida asociada a los estados
    begin
        LIGHT <= (others => '0'); --si no hay asignaci�n en case, todos los LEDs apagados
        case current_state is
            when S0 => --PRODUCTO 0 (producto sin seleccionar)
                LIGHT <= (others => '0'); --LEDs apagados
            when S1 => --PRODUCTO 1
                LIGHT <= "001"; --LEDs
            when S2 => --PRODUCTO 2
                LIGHT <= "010"; --LEDs
            when S3 => --PRODUCTO 3
                LIGHT <= "011"; --LEDs
            when S4 => --PRODUCTO 4
                LIGHT <= "100"; --LEDs
            when others => --cualquier otro caso
                LIGHT <= (others => '0'); --LEDs apagados
        end case;
    end process;   
     
end behavioral;
