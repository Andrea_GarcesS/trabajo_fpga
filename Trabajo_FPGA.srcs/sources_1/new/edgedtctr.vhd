----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.12.2020 12:35:12
-- Design Name: 
-- Module Name: edgedtctr - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity edgedtctr is
  Port(
    CLK : in std_logic; --reloj de 100 MHz
    SYNC_IN : in std_logic; --se�al del bot�n sincronizada
    EDGE : out std_logic --se�al del bot�n de salida
  );
end edgedtctr;

architecture Behavioral of edgedtctr is
    signal sreg : std_logic_vector (2 DOWNTO 0);
begin
    process(CLK)
    begin
        if rising_edge(CLK) then --cuando hay flanco de subida del reloj
            sreg <= sreg(1 DOWNTO 0) & SYNC_IN; --desplazamiento hacia la izquierda para recibir nuevo SYNC_IN en la posici�n 0
        end if;
    end process;

    with sreg select
        EDGE <= '1' when "100", --bot�n a 1 cuando SYNC_IN fue 1 y en los dos siguiente flancos de reloj fue 0
                '0' when others; --bot�n a 0 en cualquier otro caso
end Behavioral;
