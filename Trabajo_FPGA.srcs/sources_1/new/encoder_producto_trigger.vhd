----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2021 14:58:12
-- Design Name: 
-- Module Name: decoder_producto_trigger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity encoder_producto_trigger is
    Generic(
        SIZE_IN : positive := 2; --tama�o de PRODUCTO_OUT
        SIZE_OUT : positive := 3 --tama�o de TRIGGER_IN
    );
    Port(
        PRODUCTO_OUT : in std_logic_vector(SIZE_IN DOWNTO 0); --salida de fsm_producto
        TRIGGER_IN : out std_logic_vector(SIZE_OUT DOWNTO 0) --disparo que va a timer_ok               
    );
end encoder_producto_trigger;

architecture Behavioral of encoder_producto_trigger is
begin
    TRIGGER_IN <= "0000" when PRODUCTO_OUT = "000" --no se dispara TRIGGER_IN si PRODUCTO_OUT es PRODUCTO 0 (producto sin seleccionar)
                  else "0001"; --se dispara cuando PRODUCTO_OUT es PRODUCTO 1, PRODUCTO 2, PRODUCTO 3 o PRODUCTO 4
end Behavioral;