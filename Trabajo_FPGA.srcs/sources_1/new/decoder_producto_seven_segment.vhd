----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2021 17:43:39
-- Design Name: 
-- Module Name: decoder_producto_seven_segment - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_producto_seven_segment is
  Generic(
        SIZE_IN : positive:= 2; --tama�o de PRODUCTO
        SIZE_OUT : positive:= 4 --tama�o del caracter N_x
  );
  Port ( 
        PRODUCTO : in std_logic_vector(SIZE_IN DOWNTO 0); --salida de fsm_producto
        N_4 : out std_logic_vector(SIZE_OUT DOWNTO 0); --caracter del display 4 
        N_5 : out std_logic_vector(SIZE_OUT DOWNTO 0); --caracter del display 5
        N_6 : out std_logic_vector(SIZE_OUT DOWNTO 0); --caracter del display 6
        N_7 : out std_logic_vector(SIZE_OUT DOWNTO 0) --caracter del display 7       
  );
end decoder_producto_seven_segment;

architecture Behavioral of decoder_producto_seven_segment is

begin    
    process(PRODUCTO)
    begin
        case PRODUCTO is
            when "000" => --S0 (    )                
                N_7 <= "11000"; --' '
                N_6 <= "11000"; --' '
                N_5 <= "11000"; --' '
                N_4 <= "11000"; --' '
            when "001" => --S1 (P1  )
                N_7 <= "10110"; --P
                N_6 <= "00010"; --1
                N_5 <= "11000"; --' '
                N_4 <= "11000"; --' '              
            when "010" => --S2 (P2  )                
                N_7 <= "10110"; --P
                N_6 <= "00100"; --2
                N_5 <= "11000"; --' '
                N_4 <= "11000"; --' '                
            when "011" => --S3 (P3  )             
                N_7 <= "10110"; --P
                N_6 <= "00110"; --3   
                N_5 <= "11000"; --' '
                N_4 <= "11000"; --' '             
            when "100" => --S4 (P4  )
                N_7 <= "10110"; --P
                N_6 <= "01000"; --4  
                N_5 <= "11000"; --' '
                N_4 <= "11000"; --' '                 
            when others => --cualquier otro caso
                N_7 <= "11000"; --' '
                N_6 <= "11000"; --' '
                N_5 <= "11000"; --' '
                N_4 <= "11000"; --' '
        end case;
    end process;
end Behavioral;
